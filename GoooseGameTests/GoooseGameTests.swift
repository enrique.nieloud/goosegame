//
//  GoooseGameTests.swift
//  GoooseGameTests
//
//  Created by Enrique Nieloud on 04/08/2021.
//

import XCTest

class GoooseGameTests: XCTestCase {
    var rulesPrinter: RulesPrinter!
    var ruleText = ""
    
    func givenRulesPrinter() {
        rulesPrinter = GooseGameCreator.createRulesPrinter()
    }
    
    func whenPositionIs(position: Int) {
        ruleText = rulesPrinter.getRule(position: position)
    }
    
    func thenRuleMustBe(text: String) {
        XCTAssert(ruleText == text)
    }
    
    func testNumber_1() {
        givenRulesPrinter()
        whenPositionIs(position: 1)
        thenRuleMustBe(text: "Stay in space 1")
    }

    func testNumber_2() {
        givenRulesPrinter()
        whenPositionIs(position: 2)
        thenRuleMustBe(text: "Stay in space 2")
    }

    func testNumber_6() {
        givenRulesPrinter()
        whenPositionIs(position: 6)
        thenRuleMustBe(text: "The Bridge: Go to space 12")
    }

    func testNumber_12() {
        givenRulesPrinter()
        whenPositionIs(position: 12)
        thenRuleMustBe(text: "Move two spaces forward")
    }

    func testNumber_18() {
        givenRulesPrinter()
        whenPositionIs(position: 18)
        thenRuleMustBe(text: "Move two spaces forward")
    }

    func testNumber_19() {
        givenRulesPrinter()
        whenPositionIs(position: 19)
        thenRuleMustBe(text: "The Hotel: Stay for (miss) one turn")
    }

    func testNumber_31() {
        givenRulesPrinter()
        whenPositionIs(position: 31)
        thenRuleMustBe(text: "The Well: Wait until someone comes to pull you out - they then take your place")
    }

    func testNumber_42() {
        givenRulesPrinter()
        whenPositionIs(position: 42)
        thenRuleMustBe(text: "The Maze: Go back to space 39")
    }

    func testNumber_50() {
        givenRulesPrinter()
        whenPositionIs(position: 50)
        thenRuleMustBe(text: "The Prison: Wait until someone comes to release you - they then take your place")
    }

    func testNumber_51() {
        givenRulesPrinter()
        whenPositionIs(position: 51)
        thenRuleMustBe(text: "The Prison: Wait until someone comes to release you - they then take your place")
    }

    func testNumber_58() {
        givenRulesPrinter()
        whenPositionIs(position: 58)
        thenRuleMustBe(text: "Death: Return your piece to the beginning - start the game again")
    }

    func testNumber_63() {
        givenRulesPrinter()
        whenPositionIs(position: 63)
        thenRuleMustBe(text: "Finish: you ended the game")
    }
    
    func testNumber_70() {
        givenRulesPrinter()
        whenPositionIs(position: 70)
        thenRuleMustBe(text: "Move to space 53 and stay in prison for two turns")
    }
}
