//
//  GooseGameRules.swift
//  GooseGameByTDD
//
//  Created by Enrique Nieloud on 05/08/2021.
//

import Foundation

class MultipleOfSixRule : Rule {
    func predicate(position: Int) -> Bool {
        position % 6 == 0
    }
    var text: String = "Move two spaces forward"
    var order: Int = 3
}

class SixRule : Rule {
    func predicate(position: Int) -> Bool {
        position == 6
    }
    var text: String = "The Bridge: Go to space 12"
    var order: Int = 1
}

class RangeFrom50to55Rule : Rule {
    func predicate(position: Int) -> Bool {
        (50...55).contains(position)
    }
    var text: String = "The Prison: Wait until someone comes to release you - they then take your place"
    var order: Int = 2
}

class NineteenRule : Rule {
    func predicate(position: Int) -> Bool {
        position == 19
    }
    var text: String = "The Hotel: Stay for (miss) one turn"
    var order: Int = 1
}

class ThirtyoneRule : Rule {
    func predicate(position: Int) -> Bool {
        position == 31
    }
    var text: String = "The Well: Wait until someone comes to pull you out - they then take your place"
    var order: Int = 1
}

class FortytwoRule : Rule {
    func predicate(position: Int) -> Bool {
        position == 42
    }
    var text: String = "The Maze: Go back to space 39"
    var order: Int = 1
}

class FiftyEightRule : Rule {
    func predicate(position: Int) -> Bool {
        position == 58
    }
    var text: String = "Death: Return your piece to the beginning - start the game again"
    var order: Int = 1
}

class SixtyThreeRule : Rule {
    func predicate(position: Int) -> Bool {
        position == 63
    }
    var text: String = "Finish: you ended the game"
    var order: Int = 1
}

class GreaterThan63Rule : Rule {
    func predicate(position: Int) -> Bool {
        position > 63
    }
    var text: String = "Move to space 53 and stay in prison for two turns"
    var order: Int = 1
}

class GooseGameRulesProvider : RulesProvider {
    let rules: Rules = [MultipleOfSixRule(), SixRule(), NineteenRule(),
                       ThirtyoneRule(), FortytwoRule(), RangeFrom50to55Rule(),
                       FiftyEightRule(), SixtyThreeRule(), GreaterThan63Rule()]
        .sorted(by: { $0.order<$1.order })
    var defaultRule: String = "Stay in space"
    var numberOfSquares: Int = 63
    
    func rule(at index: Int) -> Rule? {
        if (0...rules.count).contains(index) {
            return rules[index]
        }
        return nil
    }
    
    var count: Int {
        get {
            return rules.count
        }
    }
}

class GooseGameCreator {
    static func createRulesPrinter() -> RulesPrinter {
        return RulesPrinter(GooseGameRulesProvider())
    }
}

