//
//  RulesPrinter.swift
//  GooseGameByTDD
//
//  Created by Enrique Nieloud on 05/08/2021.
//

import Foundation

typealias RulePredicate = (Int) -> Bool

protocol Rule {
    func predicate(position: Int) -> Bool
    var text: String { get }
    var order: Int { get }
}

typealias Rules = [Rule]

protocol RulesProvider {
    var count: Int { get }
    func rule(at index: Int) -> Rule?
    var defaultRule: String { get }
    var numberOfSquares: Int { get }
}

class RulesPrinter {
    let rulesProvider: RulesProvider
    
    init(_ rulesProvider: RulesProvider) {
        self.rulesProvider = rulesProvider
    }
    
    func evaluateRule(number ruleIndex: Int, atPosition position: Int) -> String? {
        if let rule = rulesProvider.rule(at: ruleIndex), rule.predicate(position: position) {
            return rule.text
        }
        return nil
    }

    func getRule(position: Int) -> String {
        for index in 0...rulesProvider.count - 1 {
            if let ruleText = evaluateRule(number: index, atPosition: position) {
                return ruleText
            }
        }
        return "\(rulesProvider.defaultRule) \(position)"
    }

    func printAllRules() {
        for position in 1...rulesProvider.numberOfSquares {
            printRule(position:	 position)
        }
    }
    
    func printRule(position: Int) {
        if let rule = rulesProvider.rule(at: position) {
            print(rule.text)
        }
    }
}

